# OpenCVCamera Plugin

Camera Plugin mit OpenCV Integration (aktuell nur für Android)

## Set up

1. In "Example" App Verzeichnis: `flutter upgrade && flutter packages get`
2. In "Example" App Verzeichnis: `main.dart` öffnen und
App ein erstes Mal bauen

## Android Entwicklung

Z.B.: Rechtsklick auf `android`-Verzeichnis. Kontextmenü "Flutter" => "Open android module in Android Studio"

__Zum Bauen der App:__ `main.dart`-File öffnen und App darüber starten