package io.flutter.plugins.opencv_camera;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImageDetector {

  public static Mat getMatOfBiggestRectangle(Mat rgbImage) throws IOException {

    if (rgbImage == null) {
      throw new IOException("No input file provided!");
    }

    Rect boundingRect = getBiggestRectangleBoundingRect(rgbImage);

    if (boundingRect != null) {
      Scalar color = new Scalar(255, 0, 0, .8);
      Core.rectangle(rgbImage, boundingRect.tl(), boundingRect.br(), color, 3);
    }

    return rgbImage;
  }

  public static Map<String, Double> getBiggestRectanglePositionPercentages(Mat rgbImage) throws IOException {

    int width = rgbImage.width();
    int height = rgbImage.height();

    Rect boundingRect = getBiggestRectangleBoundingRect(rgbImage);

    Map<String, Double> positions = new HashMap<String, Double>();

    double x1 = -1;
    double y1 = -1;
    double x2 = -1;
    double y2 = -1;
    if (boundingRect != null) {
      x1 = boundingRect.tl().x / width;
      y1 = boundingRect.tl().y / height;
      x2 = boundingRect.br().x / width;
      y2 = boundingRect.br().y / height;
    }

    positions.put("x1", x1);
    positions.put("y1", y1);

    positions.put("x2", x2);
    positions.put("y2", y2);

    return positions;
  }

  public static Rect getBiggestRectangleBoundingRect(Mat rgbImage) throws IOException {
    if (rgbImage == null) {
      throw new IOException("No input file provided!");
    }

    Mat destination = new Mat(rgbImage.size(), CvType.CV_8UC1);
    Imgproc.cvtColor(rgbImage, destination, Imgproc.COLOR_RGB2GRAY);

    Imgproc.equalizeHist(destination, destination);
    Imgproc.GaussianBlur(destination, destination, new Size(5, 5), 0, 0);

    int threshold = 100;

    Imgproc.Canny(destination, destination, threshold, 2 * threshold);
    Imgproc.dilate(destination, destination, new Mat(), new Point(-1, -1), 1);

    List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
    Imgproc.findContours(destination, contours, new Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

    double biggestAvailableArea = 0;
    MatOfPoint biggestAvailableContour = new MatOfPoint();

    for (MatOfPoint contour : contours) {
      double area = Imgproc.contourArea(contour);

      if (area > biggestAvailableArea) {
        MatOfPoint2f approxCurve = new MatOfPoint2f();
        MatOfPoint2f curve = new MatOfPoint2f(contour.toArray());

        if (curve.toArray().length > 0) {
          Imgproc.approxPolyDP(curve, approxCurve, 0.02 * Imgproc.arcLength(curve, true), true);

          int numberOfVertices = (int) approxCurve.total();
          if (numberOfVertices == 4) {
            biggestAvailableArea = area;
            biggestAvailableContour = contour;
          }
        }
      }
    }

    if (biggestAvailableContour.toArray().length > 0) {
      return Imgproc.boundingRect(biggestAvailableContour);
    }

    return null;
  }

}
