package io.flutter.plugins.opencv_camera;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;

import org.opencv.android.InstallCallbackInterface;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.view.FlutterView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class OpenCVCameraPlugin implements MethodCallHandler {

  private static final int CAMERA_REQUEST_ID = 513469796;
  private static final String TAG = "OpenCVCameraPlugin";
  private static final SparseIntArray ORIENTATIONS =
      new SparseIntArray() {
        {
          append(Surface.ROTATION_0, 0);
          append(Surface.ROTATION_90, 90);
          append(Surface.ROTATION_180, 180);
          append(Surface.ROTATION_270, 270);
        }
      };

  private static CameraManager cameraManager;
  private final FlutterView view;
  private Camera camera;
  private Activity activity;
  private Registrar registrar;
  // The code to run after requesting camera permissions.
  private Runnable cameraPermissionContinuation;
  private boolean requestingPermission;

  private OpenCVCameraPlugin(Registrar registrar, FlutterView view, Activity activity) {
    this.registrar = registrar;
    this.view = view;
    this.activity = activity;

    registrar.addRequestPermissionsResultListener(new CameraRequestPermissionsListener());

    activity
        .getApplication()
        .registerActivityLifecycleCallbacks(
            new Application.ActivityLifecycleCallbacks() {
              @Override
              public void onActivityCreated(Activity activity, Bundle savedInstanceState) {}

              @Override
              public void onActivityStarted(Activity activity) {}

              @Override
              public void onActivityResumed(Activity activity) {
                if (requestingPermission) {
                  requestingPermission = false;
                  return;
                }
                if (activity == OpenCVCameraPlugin.this.activity) {
                  if (camera != null) {
                    camera.open(null);
                  }
                }
                if (!OpenCVLoader.initDebug()) {
                  Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
                  boolean success = OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_13, activity, new LoaderCallbackInterface() {
                    @Override
                    public void onManagerConnected(int status) {}

                    @Override
                    public void onPackageInstall(int operation, InstallCallbackInterface callback) {}
                  });
                  if (!success)
                    Log.e("OpenCV", "Asynchronous initialization failed!");
                  else
                    Log.d("OpenCV", "Asynchronous initialization succeeded!");
                }
              }

              @Override
              public void onActivityPaused(Activity activity) {
                if (activity == OpenCVCameraPlugin.this.activity) {
                  if (camera != null) {
                    camera.close();
                  }
                }
              }

              @Override
              public void onActivityStopped(Activity activity) {
                if (activity == OpenCVCameraPlugin.this.activity) {
                  if (camera != null) {
                    camera.close();
                  }
                }
              }

              @Override
              public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

              @Override
              public void onActivityDestroyed(Activity activity) {}
            });
  }

  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "plugins.flutter.io/opencv_camera");

    cameraManager = (CameraManager) registrar.activity().getSystemService(Context.CAMERA_SERVICE);

    channel.setMethodCallHandler(new OpenCVCameraPlugin(registrar, registrar.view(), registrar.activity()));
  }

  @Override
  public void onMethodCall(MethodCall call, final Result result) {
    switch (call.method) {
      case "init":
        if (camera != null) {
          camera.close();
        }
        result.success(null);
        break;
      case "availableCameras": {
        try {
          String[] cameraNames = cameraManager.getCameraIdList();
          List<Map<String, Object>> cameras = new ArrayList<>();
          for (String cameraName : cameraNames) {
            HashMap<String, Object> details = new HashMap<>();
            CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraName);
            details.put("name", cameraName);
            @SuppressWarnings("ConstantConditions")
            int lensFacing = characteristics.get(CameraCharacteristics.LENS_FACING);
            switch (lensFacing) {
              case CameraMetadata.LENS_FACING_FRONT:
                details.put("lensFacing", "front");
                break;
              case CameraMetadata.LENS_FACING_BACK:
                details.put("lensFacing", "back");
                break;
              case CameraMetadata.LENS_FACING_EXTERNAL:
                details.put("lensFacing", "external");
                break;
            }
            cameras.add(details);
          }
          result.success(cameras);
        } catch (CameraAccessException e) {
          result.error("cameraAccess", e.getMessage(), null);
        }
        break;
      }
      case "initialize": {
          String cameraName = call.argument("cameraName");
          String resolutionPreset = call.argument("resolutionPreset");
          if (camera != null) {
            camera.close();
          }
          camera = new Camera(cameraName, resolutionPreset, result);
          break;
      }
      case "takePicture": {
          camera.analyzePicture(result);
          break;
      }
      case "dispose": {
          if (camera != null) {
            camera.dispose();
          }
          result.success(null);
          break;
      }
      default:
        result.notImplemented();
        break;
    }
  }

  private static class CompareSizesByArea implements Comparator<Size> {
    @Override
    public int compare(Size lhs, Size rhs) {
      // We cast here to ensure the multiplications won't overflow.
      return Long.signum(
          (long) lhs.getWidth() * lhs.getHeight() - (long) rhs.getWidth() * rhs.getHeight());
    }
  }

  private class CameraRequestPermissionsListener
      implements PluginRegistry.RequestPermissionsResultListener {
    @Override
    public boolean onRequestPermissionsResult(int id, String[] permissions, int[] grantResults) {
      if (id == CAMERA_REQUEST_ID) {
        cameraPermissionContinuation.run();
        return true;
      }
      return false;
    }
  }

  private class Camera {
    private final FlutterView.SurfaceTextureEntry textureEntry;
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSession;
    private EventChannel.EventSink eventSink;
    private ImageReader imageReader;
    private int sensorOrientation;
    private boolean isFrontFacing;
    private String cameraName;
    private Size captureSize;
    private Size previewSize;
    private CaptureRequest.Builder captureRequestBuilder;
    private Size videoSize;
    private MediaRecorder mediaRecorder;
    private boolean recordingVideo;

    Camera(final String cameraName, final String resolutionPreset, @NonNull final Result result) {

      this.cameraName = cameraName;
      textureEntry = view.createSurfaceTexture();

      registerEventChannel();

      try {
        Size minPreviewSize;
        switch (resolutionPreset) {
          case "high":
            minPreviewSize = new Size(1024, 768);
            break;
          case "medium":
            minPreviewSize = new Size(640, 480);
            break;
          case "low":
            minPreviewSize = new Size(320, 240);
            break;
          default:
            throw new IllegalArgumentException("Unknown preset: " + resolutionPreset);
        }

        CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraName);
        StreamConfigurationMap streamConfigurationMap =
            characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        //noinspection ConstantConditions
        sensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        //noinspection ConstantConditions
        isFrontFacing =
            characteristics.get(CameraCharacteristics.LENS_FACING) == CameraMetadata.LENS_FACING_FRONT;
        computeBestCaptureSize(streamConfigurationMap);
        computeBestPreviewAndRecordingSize(streamConfigurationMap, minPreviewSize, captureSize);

        if (cameraPermissionContinuation != null) {
          result.error("cameraPermission", "Camera permission request ongoing", null);
        }
        cameraPermissionContinuation =
            new Runnable() {
              @Override
              public void run() {
                cameraPermissionContinuation = null;
                if (!hasCameraPermission()) {
                  result.error(
                      "cameraPermission", "MediaRecorderCamera permission not granted", null);
                  return;
                }
                open(result);
              }
            };
        requestingPermission = false;
        if (hasCameraPermission()) {
          cameraPermissionContinuation.run();
        } else {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestingPermission = true;
            registrar.activity().requestPermissions(new String[] {Manifest.permission.CAMERA}, CAMERA_REQUEST_ID);
          }
        }
      } catch (CameraAccessException e) {
        result.error("CameraAccess", e.getMessage(), null);
      } catch (IllegalArgumentException e) {
        result.error("IllegalArgumentException", e.getMessage(), null);
      }
    }

    private void registerEventChannel() {
      new EventChannel(
              registrar.messenger(), "flutter.io/opencv_camera_plugin/cameraEvents" + textureEntry.id())
          .setStreamHandler(
              new EventChannel.StreamHandler() {
                @Override
                public void onListen(Object arguments, EventChannel.EventSink eventSink) {
                  Camera.this.eventSink = eventSink;
                }

                @Override
                public void onCancel(Object arguments) {
                  Camera.this.eventSink = null;
                }
              });
    }

    private boolean hasCameraPermission() {
      return Build.VERSION.SDK_INT < Build.VERSION_CODES.M
          || activity.checkSelfPermission(Manifest.permission.CAMERA)
              == PackageManager.PERMISSION_GRANTED;
    }

    private void computeBestPreviewAndRecordingSize(
        StreamConfigurationMap streamConfigurationMap, Size minPreviewSize, Size captureSize) {
      Size[] sizes = streamConfigurationMap.getOutputSizes(SurfaceTexture.class);
      float captureSizeRatio = (float) captureSize.getWidth() / captureSize.getHeight();
      List<Size> goodEnough = new ArrayList<>();
      for (Size s : sizes) {
        if ((float) s.getWidth() / s.getHeight() == captureSizeRatio
            && minPreviewSize.getWidth() < s.getWidth()
            && minPreviewSize.getHeight() < s.getHeight()) {
          goodEnough.add(s);
        }
      }

      Collections.sort(goodEnough, new CompareSizesByArea());

      if (goodEnough.isEmpty()) {
        previewSize = sizes[0];
        videoSize = sizes[0];
      } else {
        previewSize = goodEnough.get(0);

        // Video capture size should not be greater than 1080 because MediaRecorder cannot handle higher resolutions.
        videoSize = goodEnough.get(0);
        for (int i = goodEnough.size() - 1; i >= 0; i--) {
          if (goodEnough.get(i).getHeight() <= 1080) {
            videoSize = goodEnough.get(i);
            break;
          }
        }
      }
    }

    private void computeBestCaptureSize(StreamConfigurationMap streamConfigurationMap) {
      // For still image captures, we use the largest available size.
      captureSize =
          Collections.max(
              Arrays.asList(streamConfigurationMap.getOutputSizes(ImageFormat.JPEG)),
              new CompareSizesByArea());
    }

    private void open(@Nullable final Result result) {
      if (!hasCameraPermission()) {
        if (result != null) result.error("cameraPermission", "Camera permission not granted", null);
      } else {
        try {
          imageReader = ImageReader.newInstance(captureSize.getWidth(), captureSize.getHeight(), ImageFormat.YUV_420_888, 2);
          cameraManager.openCamera(
              cameraName,
              new CameraDevice.StateCallback() {
                @Override
                public void onOpened(@NonNull CameraDevice cameraDevice) {
                  Camera.this.cameraDevice = cameraDevice;
                  try {
                    startPreview();
                  } catch (CameraAccessException e) {
                    if (result != null) result.error("CameraAccess", e.getMessage(), null);
                  }

                  if (result != null) {
                    Map<String, Object> reply = new HashMap<>();
                    reply.put("textureId", textureEntry.id());
                    reply.put("previewWidth", previewSize.getWidth());
                    reply.put("previewHeight", previewSize.getHeight());
                    result.success(reply);
                  }
                }

                @Override
                public void onClosed(@NonNull CameraDevice camera) {
                  if (eventSink != null) {
                    Map<String, String> event = new HashMap<>();
                    event.put("eventType", "cameraClosing");
                    eventSink.success(event);
                  }
                  super.onClosed(camera);
                }

                @Override
                public void onDisconnected(@NonNull CameraDevice cameraDevice) {
                  cameraDevice.close();
                  Camera.this.cameraDevice = null;
                  sendErrorEvent("The camera was disconnected.");
                }

                @Override
                public void onError(@NonNull CameraDevice cameraDevice, int errorCode) {
                  cameraDevice.close();
                  Camera.this.cameraDevice = null;
                  String errorDescription;
                  switch (errorCode) {
                    case ERROR_CAMERA_IN_USE:
                      errorDescription = "The camera device is in use already.";
                      break;
                    case ERROR_MAX_CAMERAS_IN_USE:
                      errorDescription = "Max cameras in use";
                      break;
                    case ERROR_CAMERA_DISABLED:
                      errorDescription =
                          "The camera device could not be opened due to a device policy.";
                      break;
                    case ERROR_CAMERA_DEVICE:
                      errorDescription = "The camera device has encountered a fatal error";
                      break;
                    case ERROR_CAMERA_SERVICE:
                      errorDescription = "The camera service has encountered a fatal error.";
                      break;
                    default:
                      errorDescription = "Unknown camera error";
                  }
                  sendErrorEvent(errorDescription);
                }
              },
              null);
        } catch (CameraAccessException e) {
          if (result != null) result.error("cameraAccess", e.getMessage(), null);
        }
      }
    }

    private void writeToFile(ByteBuffer buffer, File file) throws IOException {
      try (FileOutputStream outputStream = new FileOutputStream(file)) {
        while (0 < buffer.remaining()) {
          outputStream.getChannel().write(buffer);
        }
      }
    }

    /**
     * Convert the Image.Plane array into a a OpenCV.Mat object in the YUV-format.
     *
     * Code taken from: https://gist.github.com/camdenfullmer/dfd83dfb0973663a7974
     *
     * @param width
     * @param height
     * @param planes
     * @return
     */
    private Mat yuvImagePlanesToYUVMat(int width, int height, Image.Plane[] planes) {
      ByteBuffer buffer;
      int rowStride;
      int pixelStride;
      int offset = 0;

      // TODO: Optimize code concerning bits per pixel and if-else-cases as we always should have the same size?!
      byte[] data = new byte[width * height * ImageFormat.getBitsPerPixel(ImageFormat.YUV_420_888) / 8];
      byte[] rowData = new byte[planes[0].getRowStride()];

      for (int i = 0; i < planes.length; i++) {
        buffer = planes[i].getBuffer();
        rowStride = planes[i].getRowStride();
        pixelStride = planes[i].getPixelStride();
        int w = (i == 0) ? width : width / 2;
        int h = (i == 0) ? height : height / 2;
        for (int row = 0; row < h; row++) {
          int bytesPerPixel = ImageFormat.getBitsPerPixel(ImageFormat.YUV_420_888) / 8;
          if (pixelStride == bytesPerPixel) {
            int length = w * bytesPerPixel;
            buffer.get(data, offset, length);

            // Advance buffer the remainder of the row stride, unless on the last row.
            // Otherwise, this will throw an IllegalArgumentException because the buffer
            // doesn't include the last padding.
            if (h - row != 1) {
              buffer.position(buffer.position() + rowStride - length);
            }
            offset += length;
          } else {

            // On the last row only read the width of the image minus the pixel stride
            // plus one. Otherwise, this will throw a BufferUnderflowException because the
            // buffer doesn't include the last padding.
            if (h - row == 1) {
              buffer.get(rowData, 0, width - pixelStride + 1);
            } else {
              buffer.get(rowData, 0, rowStride);
            }

            for (int col = 0; col < w; col++) {
              data[offset++] = rowData[col * pixelStride];
            }
          }
        }
      }

      // Finally, create the Mat.
      Mat mat = new Mat(height + height / 2, width, CvType.CV_8UC1);
      mat.put(0, 0, data);

      return mat;
    }

    private void analyzePicture(@NonNull final Result result) {
      final String TAG = "OpenCV Image Processing";

        imageReader.setOnImageAvailableListener(
          new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(final ImageReader reader) {

              // Acquire the latest image in the buffer and make sure it is in YUV_420-format
              Image image = null;
              try {
                image = reader.acquireLatestImage();
              } catch (Exception e) {
                result.error(TAG + ": Acquire Latest Image", e.getMessage() + "\n" + e.getStackTrace().toString() + "\n" + e.getCause(), null);
                if (image != null) {
                  image.close();
                }
                return;
              }

              // Create a separate thread, as this might block the UI-thread otherwise
              Thread imageProcessingThread = new ImageProcessingThread(result, image, reader) {
                @Override
                public void run() {

                  Image.Plane[] planes = null;
                  Map<String, Double> positions = new HashMap<String, Double>();
                  try {
                    if(image.getFormat() != ImageFormat.YUV_420_888) {
                      throw new IllegalArgumentException("Image must have format YUV_420_888");
                    }

                    planes = image.getPlanes();
                    if (planes[1].getPixelStride() != 1 && planes[1].getPixelStride() != 2) {
                      throw new IllegalArgumentException("Chrome plane pixel stride of image must be of 1 or 2. Got " + planes[1].getPixelStride());
                    }

                    // Convert the raw image data to a OpenCV.Mat-object and transfer the color space to rgba
                    Mat yuvMat = yuvImagePlanesToYUVMat(image.getWidth(), image.getHeight(), planes);
                    Mat bgrMat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC4);
                    Imgproc.cvtColor(yuvMat, bgrMat, Imgproc.COLOR_YUV2BGR_I420);

                    // Detect possible rectangles and return the edge-positions of the biggest one
                    positions.putAll(ImageDetector.getBiggestRectanglePositionPercentages(bgrMat));

                    result.success(positions);

                  } catch (IllegalArgumentException exception) {
                    Log.e(TAG, exception.getMessage() + "\n\n\n" + exception.getStackTrace().toString() + "\n\n\n" + exception.getCause());
                    result.error(TAG, exception.getMessage(), null);
                  } catch (IOException exception) {
                    Log.e(TAG, "ImageProcessingError \n" + exception.getMessage() + "\n\n\n" + exception.getStackTrace().toString() + "\n\n\n" + exception.getCause());
                    result.error(TAG +": Image Processing Error", exception.getMessage(), null);
                  } catch (Exception exception) {
                    Log.e(TAG, exception.getMessage() + "\n\n\n" + exception.getStackTrace().toString() + "\n\n\n" + exception.getCause());
                    result.error(TAG, exception.getMessage(), null);
                  } finally {
                    // Release the resources
                    if (image != null) {
                      image.close();
                    }
                  }
                }
              };

              imageProcessingThread.start();
            }
        }, null);


        try {
          final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
          captureBuilder.addTarget(imageReader.getSurface());
          int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
          int displayOrientation = ORIENTATIONS.get(displayRotation);
          if (isFrontFacing){
            displayOrientation = -displayOrientation;
          }

          captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, (-displayOrientation + sensorOrientation) % 360);

          cameraCaptureSession.capture(
              captureBuilder.build(),
              new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureFailed(
                    @NonNull CameraCaptureSession session,
                    @NonNull CaptureRequest request,
                    @NonNull CaptureFailure failure) {
                  String reason;
                  switch (failure.getReason()) {
                    case CaptureFailure.REASON_ERROR:
                      reason = "An error happened in the framework";
                      break;
                    case CaptureFailure.REASON_FLUSHED:
                      reason = "The capture has failed due to an abortCaptures() call";
                      break;
                    default:
                      reason = "Unknown reason";
                  }
                  result.error("captureFailure", reason, null);
                }
              },
              null);
        } catch (CameraAccessException e) {
          result.error("cameraAccess", e.getMessage(), null);
        }
    }

    private void startPreview() throws CameraAccessException {
      closeCaptureSession();

      SurfaceTexture surfaceTexture = textureEntry.surfaceTexture();
      surfaceTexture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
      captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

      List<Surface> surfaces = new ArrayList<>();

      Surface previewSurface = new Surface(surfaceTexture);
      surfaces.add(previewSurface);
      captureRequestBuilder.addTarget(previewSurface);

      surfaces.add(imageReader.getSurface());

      cameraDevice.createCaptureSession(
          surfaces,
          new CameraCaptureSession.StateCallback() {

            @Override
            public void onConfigured(@NonNull CameraCaptureSession session) {
              if (cameraDevice == null) {
                sendErrorEvent("The camera was closed during configuration.");
                return;
              }
              try {
                cameraCaptureSession = session;
                captureRequestBuilder.set(
                    CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
                cameraCaptureSession.setRepeatingRequest(
                    captureRequestBuilder.build(),
                    new CameraCaptureSession.CaptureCallback() {
                      @Override
                      public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
                        super.onCaptureStarted(session, request, timestamp, frameNumber);
                      }
                    }, null);
              } catch (CameraAccessException e) {
                sendErrorEvent(e.getMessage());
              }
            }

            @Override
            public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
              sendErrorEvent("Failed to configure the camera for preview.");
            }
          },
          null);
    }

    private void sendErrorEvent(String errorDescription) {
      if (eventSink != null) {
        Map<String, String> event = new HashMap<>();
        event.put("eventType", "error");
        event.put("errorDescription", errorDescription);
        eventSink.success(event);
      }
    }

    private void closeCaptureSession() {
      if (cameraCaptureSession != null) {
        cameraCaptureSession.close();
        cameraCaptureSession = null;
      }
    }

    private void close() {
      closeCaptureSession();

      if (cameraDevice != null) {
        cameraDevice.close();
        cameraDevice = null;
      }
      if (imageReader != null) {
        imageReader.close();
        imageReader = null;
      }
      if (mediaRecorder != null) {
        mediaRecorder.reset();
        mediaRecorder.release();
        mediaRecorder = null;
      }
    }

    private void dispose() {
      close();
      textureEntry.release();
    }
  }
}
