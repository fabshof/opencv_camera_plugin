package io.flutter.plugins.opencv_camera;

import io.flutter.plugin.common.MethodChannel.Result;

import android.media.Image;
import android.media.ImageReader;

public class ImageProcessingThread extends Thread {
  Image image;
  Result  result;
  ImageReader reader;
  ImageProcessingThread(Result result, Image image, ImageReader reader) {
    this.image = image;
    this.result = result;
    this.reader = reader;
  }
}
