import 'package:flutter/material.dart';
import 'dart:async';

class Debouncer {
  Duration delay;
  Function callback;

  Debouncer({this.delay, this.callback});

  Timer _timeoutId = null;

  void debounce() {

    //cancel the previous timer if debounce is still being called before the delay period is over
    if(_timeoutId != null) {
      _timeoutId.cancel();
    }
    //schedule a new call after delay time
    _timeoutId = new Timer(
      delay,
      () {
        callback();
        _timeoutId = null;
      }
    );
  }
}