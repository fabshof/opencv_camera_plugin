import 'dart:async';

import 'package:opencv_camera/opencv_camera.dart';
import 'package:flutter/material.dart';

import 'package:opencv_camera/bounding_box.dart';
import 'util/debouncer.dart';

class CameraExampleHome extends StatefulWidget {
  @override
  _CameraExampleHomeState createState() {
    return new _CameraExampleHomeState();
  }
}

/// Returns a suitable camera icon for [direction].
IconData getCameraLensIcon(CameraLensDirection direction) {
  switch (direction) {
    case CameraLensDirection.back:
      return Icons.camera_rear;
    case CameraLensDirection.front:
      return Icons.camera_front;
    case CameraLensDirection.external:
      return Icons.camera;
  }
  throw new ArgumentError('Unknown lens direction');
}

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

class _CameraExampleHomeState extends State<CameraExampleHome> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  CameraController controller;
  BoundingBox _boundingBox;
  bool _isDisplayTarget = false;
  bool _isButtonEnabled = true;

  @override
  Widget build(BuildContext context) {

    double padding = 1.0;
    Size size = MediaQuery.of(context).size;
    double containerHeight = .75 * size.height;
    double containerWidth = size.width - (2 * padding);

    double defaultThreshold = 5.0;
    double left = (this._boundingBox != null) ? containerWidth * this._boundingBox.x1 : defaultThreshold;
    double top = (this._boundingBox != null) ? containerHeight * this._boundingBox.y1 : defaultThreshold;
    double right = (this._boundingBox != null) ? containerWidth * (1 - this._boundingBox.x2) : containerWidth - defaultThreshold;
    double bottom = (this._boundingBox != null) ? containerHeight * (1 - this._boundingBox.y2) : containerHeight - defaultThreshold;

    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: const Text('Camera example'),
      ),
      body: new Column(
        children: <Widget>[
          Container(
            height: containerHeight,
            child: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: Padding(
                    padding: EdgeInsets.all(padding),
                    child: _cameraPreviewWidget(),
                  )
                ),
                Positioned.fill(
                  child: Padding(
                    padding: EdgeInsets.all(padding),
                    child: _targetWidget(padding: padding, left: left, top: top, right: right, bottom: bottom),
                  ),
                )

              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                _cameraTogglesRowWidget()
              ],
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: FlatButton(
          child: _isButtonEnabled ? Icon(Icons.photo_camera) : Icon(Icons.block),
          onPressed: (controller != null && controller.value.isInitialized && _isButtonEnabled) ? () {
            if (_isButtonEnabled) {
              setState(() {
                _isButtonEnabled = false;
                _isDisplayTarget = false;
                takePicture()
                  .then((BoundingBox boundingBox) {
                    showInSnackBar('Found rectangle!');
                      this._boundingBox = boundingBox;
                      this._isDisplayTarget = true;
                      this._isButtonEnabled = true;
                  })
                  .catchError((e) {
                    print('Error: ${e.error}');
                  });
              });

            }
          } : null,
        ),
      ),
    );
  }

  Widget _targetWidget({double padding = 0.0, double left, double top, double right, double bottom, }) {
    return Opacity(
      opacity: this._isDisplayTarget ? 1.0 : 0.0,
      child: CustomPaint(
        painter: TargetPainter(
        left: left,
        top: top,
        right: right,
        bottom: bottom
        ),
      )
    );
  }

  /// Display the preview from the camera (or a message if the preview is not available).
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return Center(
        child: const Text(
          'Tap a camera',
          style: const TextStyle(
            color: Colors.yellow,
            fontSize: 24.0,
            fontWeight: FontWeight.w900,
          ),
        ),
      );
    } else {
      return new AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: new CameraPreview(controller),
      );
    }
  }

  /// Display a row of toggle to select the camera (or a message if no camera is available).
  Widget _cameraTogglesRowWidget() {
    final List<Widget> toggles = <Widget>[];

    if (cameras.isEmpty) {
      return const Text('No camera found');
    } else {
      for (CameraDescription cameraDescription in cameras) {
        toggles.add(
          new SizedBox(
            width: 90.0,
            child: new RadioListTile<CameraDescription>(
              title:
                  new Icon(getCameraLensIcon(cameraDescription.lensDirection)),
              groupValue: controller?.description,
              value: cameraDescription,
              onChanged: controller != null && controller.value.isRecordingVideo
                  ? null
                  : onNewCameraSelected,
            ),
          ),
        );
      }
    }

    return new Row(children: toggles);
  }

  String timestamp() => new DateTime.now().millisecondsSinceEpoch.toString();

  void showInSnackBar(String message) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(message)));
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = new CameraController(cameraDescription, ResolutionPreset.high);

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  Future<BoundingBox> takePicture() async {
    if (!controller.value.isInitialized) {
      showInSnackBar('Error: select a camera first.');
      return null;
    }

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      return await controller.takePicture();
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
  }

  void _showCameraException(CameraException e) {
    logError(e.code, e.description);
    showInSnackBar('Error: ${e.code}\n${e.description}');
  }
}

class TargetPainter extends CustomPainter {

  double left;
  double top;
  double right;
  double bottom;

  TargetPainter({this.left, this.top, this.right, this.bottom});

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawRect(
        Rect.fromLTRB(this.left, this.top, this.right, this.bottom),
        Paint()
          ..color = Colors.redAccent
          ..style = PaintingStyle.stroke
          ..strokeWidth = 1.5
    );
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class CameraApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new CameraExampleHome(),
    );
  }
}

List<CameraDescription> cameras;

Future<Null> main() async {
  // Fetch the available cameras before initializing the app.
  try {
    cameras = await availableCameras();
  } on CameraException catch (e) {
    logError(e.code, e.description);
  }
  runApp(new CameraApp());
}
