///
/// The bounding box object. Takes the percentages of the upper left corner (x1, x2)
/// and the bottom right one (x2, y2) of an imaginary rectangle in respect to the
/// whole container width/ height. The coordinate system starts at the top left,
/// positive x-values are located right, positive y-values are located downwards
/// relative to the origin.
///  ––––––––––> x
/// |  o (x1, y1)
/// |
/// |
/// |          o (x2, y2)
/// V
/// y
///
class BoundingBox {
  double x1;
  double y1;
  double x2;
  double y2;

  BoundingBox({this.x1, this.y1, this.x2, this.y2}) :
        assert((x1 is double) && (x1 >= 0) && (x1 < x2), 'Given: $x1 of type ${x1.runtimeType}'),
        assert((y1 is double) && (y1 >= 0) && (y1 < y2), 'Given: $y1 of type ${y1.runtimeType}'),
        assert((x2 is double) && (x2 >= 0), 'Given: $x2 of type ${x2.runtimeType}'),
        assert((y2 is double) && (y2 >= 0), 'Given: $y2 of type ${y2.runtimeType}');

  @override
  String toString() {
    return 'BoundingBox: 1($x1, $y1) 2($x2, $y2)';
  }
}